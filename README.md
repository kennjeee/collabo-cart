# README

Clone the repository
`git clone https://kennjeee@bitbucket.org/kennjeee/collabo-cart.git`

Install the gem
`bundle install`

Creating db tables
`rails db:migrate`

Run the server
`rails server`

Currently, there are no data on the db, I have not prepopulated the data upon start up so please help yourself to add data related to this app

Add new restaurant then add new Menu for that restaurant

Add new restaurant
http://localhost:3000/restaurants

Add new menu
http://localhost:3000/menus

### Group Cart ###

1. Create a new Group Cart
2. Put group cart's name and maximum price per person.
3. Create Cart
4. Copy and paste the url localhost:3000/carts/:id to other participants, note that they are also required to sign up/login before adding items.
