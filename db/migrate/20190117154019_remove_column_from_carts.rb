class RemoveColumnFromCarts < ActiveRecord::Migration[5.1]
  def change
    remove_reference :carts, :created_by, foreign_key: true
  end
end
