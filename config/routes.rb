Rails.application.routes.draw do
  resources :cart_items do 
    get '/remove_from_cart', to: 'cart_items#remove_from_cart', as: 'remove_from_cart'
  end
  resources :carts do 
    get 'checkout'
  end
  resources :restaurants
  resources :menus do
    get '/:cart_id/add_to_cart', to: 'menus#add_to_cart', as: 'add_to_cart'
  end
  resources :home
  root 'home#index'
  
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'newrest', to: 'restaurants#new', as: 'newrest'
  get 'newmenu', to: 'menus#new', as: 'newmenu'
end