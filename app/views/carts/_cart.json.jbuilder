json.extract! cart, :id, :name, :active, :created_at, :updated_at
json.url cart_url(cart, format: :json)
