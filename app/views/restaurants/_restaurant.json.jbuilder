json.extract! restaurant, :id, :name, :has_many, :, :created_at, :updated_at
json.url restaurant_url(restaurant, format: :json)
