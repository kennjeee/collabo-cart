json.extract! menu, :id, :name, :restaurant_id, :price, :created_at, :updated_at
json.url menu_url(menu, format: :json)
