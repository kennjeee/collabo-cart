class HomeController < ApplicationController
  def index        
    unless current_user.nil?
      cart_items = CartItem.joins("INNER JOIN carts on carts.id = cart_items.cart_id AND carts.active = 't'").where(user_id:current_user.id).first
      @history = Cart.select("carts.id as id, restaurants.name as name, date(carts.created_at) as created_at, sum(menus.price) as total_price, carts.active as status")
      .joins("INNER JOIN cart_items on cart_items.cart_id=carts.id")
      .joins("INNER JOIN menus on menus.id=cart_items.menu_id")
      .joins("INNER JOIN restaurants on restaurants.id=menus.restaurant_id").where(user_id: current_user.id).group("carts.id")
      if cart_items != nil 
        puts cart_items.cart_id
        @carts = Cart.where(id:cart_items.cart_id, active: true).first        
      else
        @carts = Cart.where(user_id:current_user.id, active: true).first
      end
    end
  end
end
