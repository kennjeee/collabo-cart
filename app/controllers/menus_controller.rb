class MenusController < ApplicationController
  before_action :set_menu, only: [:show, :edit, :update, :destroy]

  # GET /menus
  # GET /menus.json
  def index
    @menus = Menu.all
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
  end

  # GET /menus/new
  def new
    @menu = Menu.new
    @restaurants = Restaurant.all
  end

  # GET /menus/1/edit
  def edit
  end

  def add_to_cart
    respond_to do |format|
      id = params[:menu_id]
      cart_id = params[:cart_id]
      cart = Cart.find(cart_id)
      menu = Menu.find(id)
      if cart.max >= menu.price 
        user_id = current_user.id
        cart_item = CartItem.new
        cart_item.user_id=user_id
        cart_item.menu_id=id
        cart_item.cart_id=cart_id
        if cart_item.save 
          format.html { redirect_to '/', notice: 'Menu was successfully added.' }
          format.json { render :show, status: :ok, location: '/' }
        else
          format.html { render :edit }
          format.json { render json: @menu.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to '/carts/' + cart_id, notice: 'Menu price exceeds price limit set by owner' }
        format.json { render :show, status: :ok, location: '/' }
      end
    end
  end

  # POST /menus
  # POST /menus.json
  def create
    @menu = Menu.new(menu_params)

    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: 'Menu was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to menus_url, notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:name, :restaurant_id, :price)
    end
end
